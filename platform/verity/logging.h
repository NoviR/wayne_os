// Copyright (c) 2010 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by the GPL v2 license that can
// be found in the LICENSE file.
//
// Provides a crude google-log compatible logging facility.
#ifndef VERITY_LOGGING_H_
#define VERITY_LOGGING_H_

#include "verity/logging/logging.h"
#define INIT_LOGGING(name, flags...) { }

#endif   // VERITY_LOGGING_H_
