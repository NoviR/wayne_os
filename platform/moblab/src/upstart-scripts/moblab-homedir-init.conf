# Copyright (c) 2014 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

description   "Ensure moblab home directory exists."
author        "chromium-os-dev@chromium.org"

start on ( stopped moblab-external-settings-init and
           stopped moblab-autotest-setup )

normal exit 0

script
  mkdir -p /var/log/bootup/
  exec >>/var/log/bootup/${UPSTART_JOB}.log 2>&1
  set -x
  set -e
  # In case stateful is wiped, recreate the moblab home directory and ensure
  # moblab owns it.
  MOBLAB_HOME=/home/moblab
  mkdir -p "${MOBLAB_HOME}"

  chown -R moblab:moblab "${MOBLAB_HOME}"
  # Copy over the ssh keys if they don't already exist.
  MOBLAB_SSH_DIR=/home/moblab/.ssh
  if [ ! -e "${MOBLAB_SSH_DIR}"/id_rsa ]; then
    mkdir -p "${MOBLAB_SSH_DIR}"
    cp /root/.ssh/* "${MOBLAB_SSH_DIR}"
    # since we romoved inbound ssh for root user, some files
    # are copied from /usr/share instead.
    cp /usr/share/chromeos-ssh-config/keys/* "${MOBLAB_SSH_DIR}"
    chown -R moblab:moblab "${MOBLAB_SSH_DIR}"
    if [ -e "${MOBLAB_SSH_DIR}"/id_rsa ]; then
      chmod 600 ${MOBLAB_SSH_DIR}/id_rsa
    fi
    if [ -e "${MOBLAB_SSH_DIR}"/mobbase_id_rsa ]; then
      chmod 600 ${MOBLAB_SSH_DIR}/mobbase_id_rsa
    fi
  fi

  if [ -d "/mnt/moblab-settings/site-settings/moblabhome" ]; then
    /usr/bin/rsync --ignore-existing -og --chown=moblab:moblab \
      /mnt/moblab-settings/site-settings/moblabhome/.??* /home/moblab
  fi

  # Create a placeholder for the boto file.
  MOBLAB_BOTO_FILE="/home/moblab/.boto"
  if [ ! -f "${MOBLAB_BOTO_FILE}" ]; then
     touch "${MOBLAB_BOTO_FILE}"
  fi

  # Create a placeholder for the service account file.
  MOBLAB_SERVICE_ACCOUNT_FILE="/home/moblab/.service_account.json"
  if [ ! -f "${MOBLAB_SERVICE_ACCOUNT_FILE}" ]; then
    touch "${MOBLAB_SERVICE_ACCOUNT_FILE}"
  fi

  # Create a placeholder for the Launch Control key file.
  MOBLAB_LAUNCH_CONTROL_KEY_FILE="/home/moblab/.launch_control_key"
  if [ ! -f "${MOBLAB_LAUNCH_CONTROL_KEY_FILE}" ]; then
    touch "${MOBLAB_LAUNCH_CONTROL_KEY_FILE}"
  fi

  chown moblab:moblab "${MOBLAB_BOTO_FILE}"
  chmod 660 "${MOBLAB_BOTO_FILE}"
  chown moblab:moblab "${MOBLAB_SERVICE_ACCOUNT_FILE}"
  chmod 660 "${MOBLAB_SERVICE_ACCOUNT_FILE}"
  chown moblab:moblab "${MOBLAB_LAUNCH_CONTROL_KEY_FILE}"
  chmod 660 "${MOBLAB_LAUNCH_CONTROL_KEY_FILE}"

  # Generate identifier for this instance of moblab, used with
  # communication with the cloud service (CPCon/results offload)
  /etc/moblab/tools/create-moblab-id.py

  logger -t "${UPSTART_JOB}" "Ending."
end script
