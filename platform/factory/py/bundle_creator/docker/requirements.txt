PyYAML==3.11
google-cloud==0.27.0
google-cloud-pubsub==0.39.0
google-auth-httplib2==0.0.3
google-api-python-client==1.6.4
