#!/bin/bash -e
# Copyright (c) 2014 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#
# Render a text file into PNG (bitmap) and SVG (vector) files.  Files named
# as '*.txt' are normal text files, and '*.TXT' files may contain markup.
#
# Options:
#
#    --lan=LANGUAGE     Render language (locale) settings
#    --font=FONTNAME    Use specified font (instead of Droid Sans)
#    --point=POINT      Font size, in points (72dpi)
#    --margin=MARGIN    Set different margin (usually for font bitmaps)
#    --color=COLOR      Override foreground color (in '#ffffff' format)
#    --align=ALING      Override align settings (default: center)
#    --markup           Render text as pango-view markup file
#    --outdir=OUTDIR    Directory for output files
#
font="Noto Sans"
language=""
pointsize=14
margin=3
align="center"
bgcolor="#ffffff"
color="#000000"
params=""
outdir=""

while true ; do
  case "$1" in
    --lan=* | --language=*)
      language="--language=${1##*=}"
      shift
      ;;
    --font=*)
      # Allows empty string = default font.
      font_params="${1##*=}"
      # TODO(hungte) support "+XXX" = add font description (ex, BOLD).
      found_font="$(IFS=','; for param in ${font_params}; do
                      if fc-list -q "${param}"; then
                        echo "${param}"
                        break
                      fi
                    done)"
      if [ -n "${font_params}" -a -z "${found_font}" ]; then
        echo "ERROR: Missing font ${font_params}." >&2
        exit 1
      fi
      font="${found_font}"
      shift
      ;;
    --align=*)
      align="${1##*=}"
      shift
      ;;
    --color=*)
      color="${1##*=}"
      shift
      ;;
    --point=*)
      pointsize="${1##*=}"
      shift
      ;;
    --margin=*)
      margin="${1##*=}"
      shift
      ;;
    --outdir=*)
      outdir="${1##*=}"
      shift
      ;;
    --markup)
      params="$params --markup"
      shift
      ;;
    *)
      break
      ;;
  esac
done

# Revise color. pango-view color syntax (in #rrggbb) may cause problem in shell
# scripts, so let's also allow 'rrggbb'.
color="${color###}"
bgcolor="${bgcolor###}"
[ -z "$color" ] || color="#$color"
[ -z "$bgcolor" ] || bgcolor="#$bgcolor"

# Image parameters
# - New pango-view has --pixel to assign font size in pixel, but that is not
#   supported by old (ex, 1.24.5 in chroot) so we must assign --dpi for
#   pointsize.
# - SVG defaults to 90DPI, which is larger than standard point size (72DPI).
#   To support both SVG and raster rendering, we always use 90DPI.

if [ -n "$outdir" ] && [ -n "$language" ]; then
  mkdir -p "${outdir%/}/lo"
fi

for txtfile in $*; do
  # pango-view does not support assigning output format options for bitmap, so
  # we must create images in PNG format and then post-process it (ex, convert
  # into BMP by ImageMagick).
  pngfile="${txtfile%.*}".png
  if [ -n "$outdir" ]; then
    pngfile="${outdir%/}/$(basename "$pngfile")"
    pngfile_lo="${outdir%/}/lo/$(basename "$pngfile")"
  fi
  svgfile="${pngfile%.*}".svg
  file_opt=""
  case "$txtfile" in
    *.txt)
      file_opt=""
      ;;
    *.TXT)
      file_opt="--markup "
      ;;
    *)
      echo "Ignoring $txtfile. Filename should end with .txt or .TXT"
      continue
      ;;
  esac
  pango_opt="-q $language \
    '--background=$bgcolor' '--foreground=$color' \
    '--font=$font $pointsize' \
    '--margin=$margin' \
    '--align=$align' \
    $params $file_opt"

  eval pango-view $pango_opt --hinting=full --dpi=180 --output "$pngfile" "$txtfile"
  if [ -n "$language" ]; then
    eval pango-view $pango_opt --hinting=full --dpi=90 --output "$pngfile_lo" "$txtfile"
  fi
  eval pango-view $pango_opt --hinting=none --output "$svgfile" "$txtfile"
  echo "wrote $pngfile,svg"
done
