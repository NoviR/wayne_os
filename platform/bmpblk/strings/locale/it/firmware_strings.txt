Chrome OS mancante o danneggiato.
Inserisci una scheda SD o una chiavetta USB di ripristino.
Inserisci una chiavetta USB di ripristino.
Inserisci una scheda SD o una chiavetta USB di ripristino (nota: la porta USB blu NON funziona per il ripristino).
Inserisci una chiavetta USB di ripristino in una delle quattro porte sul RETRO del dispositivo.
Il dispositivo inserito non contiene Chrome OS.
La verifica del sistema operativo NON È ATTIVA.
Premi la BARRA SPAZIATRICE per riattivarla.
Premi INVIO per confermare l'attivazione della verifica del sistema operativo.
Il sistema verrà riavviato e i dati locali verranno cancellati.
Per tornare indietro, premi ESC.
La verifica del sistema operativo è ATTIVA.
Per DISATTIVARE la verifica del sistema operativo, premi INVIO.
Per assistenza, visita la pagina https://google.com/chromeos/recovery
Codice di errore
Rimuovi tutti i dispositivi esterni per iniziare il ripristino.
Modello 60061e
Per DISATTIVARE la verifica del sistema operativo, premi il pulsante RIPRISTINO.
L'alimentatore collegato non fornisce energia sufficiente per consentire il funzionamento del dispositivo.
Chrome OS non verrà arrestato.
Utilizza l'alimentatore adeguato e riprova.
Rimuovi tutti i dispositivi collegati e avvia il ripristino.
Premi un tasto numerico per selezionare un bootloader alternativo:
Premi il tasto di ACCENSIONE per eseguire l'analisi diagnostica.
Per disattivare la verifica del sistema operativo, premi il tasto di ACCENSIONE.
