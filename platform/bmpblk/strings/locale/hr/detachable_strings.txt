Opcije za razvojne programere
Prikaz informacija o otklanjanju pogrešaka
Omogući potvrdu OS-a
Isključivanje
Jezik
Pokretanje s mreže
Pokretanje legacy BIOS-a
Pokretanje s USB-a
Pokretanje s USB-a ili SD kartice
Pokretanje s unutarnjeg diska
Odustani
Potvrdi omogućivanje potvrde OS-a
Onemogući potvrdu OS-a
Potvrdi onemogućivanje potvrde OS-a
Za kretanje prema gore ili dolje koristite se tipkama za glasnoću,
a za odabir opcije upotrijebite tipku za uključivanje/isključivanje.
Onemogućivanje potvrde OS-a učinit će vaš sustav NESIGURNIM.
Odaberite "Odustani" da biste ostali zaštićeni.
Potvrda OS-a isključena je. Vaš sustav NIJE SIGURAN.
Odaberite "Omogući potvrdu OS-a" da biste vratili zaštitu.
Odaberite "Potvrdi omogućivanje potvrde OS-a" da biste zaštitili svoj sustav.
