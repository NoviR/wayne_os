Opcje programisty
Pokaż dane debugowania
Włącz weryfikację systemu operacyjnego
Wyłącz zasilanie
Język
Rozruch z sieci
Rozruch ze starszej wersji systemu BIOS
Rozruch z dysku USB
Rozruch z dysku USB lub karty SD
Rozruch z dysku wewnętrznego
Anuluj
Potwierdź włączenie weryfikacji systemu operacyjnego
Wyłącz weryfikację systemu operacyjnego
Potwierdź wyłączenie weryfikacji systemu operacyjnego
Użyj przycisków głośności, by przechodzić w górę lub w dół,
i przycisku zasilania, by wybrać opcję.
Wyłączenie weryfikacji systemu operacyjnego sprawi, że system będzie NIEZABEZPIECZONY.
Wybierz „Anuluj”, by zachować ochronę.
Weryfikacja systemu operacyjnego jest WYŁĄCZONA. System jest NIEZABEZPIECZONY.
Wybierz „Włącz weryfikację systemu operacyjnego”, by przywrócić ochronę.
Wybierz „Potwierdź włączenie weryfikacji systemu operacyjnego”, by chronić system.
