# Copyright (c) 2014 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
from matplotlib import pyplot
import numpy as np
import scipy.signal as signal

from optofidelity.util import const_property

from .events import (AnalogStateEvent, Event, FingerEvent, LEDEvent,
                     LineDrawEvent, ScreenDrawEvent)


class Trace(list):
  """Contains a list of events ordered by time.

  Allows the trace to be modified or information to be extracted from it.
  It exposes multiple time-series arrays plotting events over time.
  """

  SPEED_SMOOTHING_KERNEL = 8
  """Kernel size for smoothing the movement in linear motion detection."""

  LINEAR_MOTION_MAX_SPEED_DEVIATION = 0.2
  """How much can the speed deviate from the target speed to be considered
     part of the linear motion area."""

  STATIONARY_MAX_SPEED = 0.5
  """Maximum speed of the finger to be considered stationary."""

  ms_per_frame = 3.33
  """Milliseconds per camera frame. Default value for older traces
     loaded from pickle files."""

  def __init__(self, iterable=[], ms_per_frame=3.33):
    super(Trace, self).__init__(iterable)
    self.ms_per_frame = ms_per_frame

  def __str__(self):
    return "\n".join([str(e) for e in self
                             if not isinstance(e, AnalogStateEvent)])

  def __repr__(self):
    return str(self)

  @property
  def start_time(self):
    """:returns int: start time of first event."""
    if len(self) > 0:
      return self[0].start_time or self[0].time
    return 0

  @property
  def end_time(self):
    """:returns int: end time of last event."""
    if len(self) > 0:
      return self[-1].time
    return 0

  @property
  def finger(self):
    """:returns np.ndarray: time-series array of the calibrated finger
                location."""
    return self._TimeSeries(FingerEvent, interpolate=True, calib=True)

  @property
  def uncalib_finger(self):
    """:returns np.ndarray: time-series array of the calibrated finger
                location."""
    return self._TimeSeries(FingerEvent, interpolate=True, calib=False)

  @property
  def finger_speed(self):
    """:returns np.ndarray: time-series array of finger speed."""
    return self._CalculateSpeed(self.finger)

  @property
  def uncalib_finger_speed(self):
    """:returns np.ndarray: time-series array of finger speed."""
    return self._CalculateSpeed(self.finger)

  @property
  def line_draw_start(self):
    """:returns np.ndarray: time-series array of where line draws start."""
    return self._TimeSeries(LineDrawEvent, True, calib=True)

  @property
  def line_draw_end(self):
    """:returns np.ndarray: time-series array of where line draws end."""
    return self._TimeSeries(LineDrawEvent, False, calib=True)

  @property
  def screen_draw_start(self):
    """:returns np.ndarray: binary time-series of when screen draws start."""
    return self._TimeSeries(ScreenDrawEvent, True, calib=True)

  @property
  def screen_draw_end(self):
    """:returns np.ndarray: binary time-series of when screen draws end."""
    return self._TimeSeries(ScreenDrawEvent, False, calib=True)

  @property
  def analog_state(self):
    return self._TimeSeries(AnalogStateEvent, True)

  @property
  def led(self):
    """:returns np.ndarray: time-series of how many LEDs are on at any given
                time."""
    return self._TimeSeries(LEDEvent, calib=True)

  @property
  def uncalib_led(self):
    """:returns np.ndarray: time-series of how many LEDs are on at any given
                time."""
    return self._TimeSeries(LEDEvent, calib=False)

  def ApplyFingerLocationCalib(self, location_offset):
    for event in self.FilteredByType(FingerEvent):
      event.location = event.uncalib_location + location_offset

  def ApplyLEDCalib(self, on_delay, off_delay):
    for event in self.FilteredByType(LEDEvent):
      if event.state is LEDEvent.STATE_ON:
        event.time = int(event.time + on_delay)
      else:
        event.time = int(event.time + off_delay)

  def InMS(self, frame_index):
    """:returns float: timestamp in milliseconds of the specified frame index.
    """
    return frame_index * self.ms_per_frame

  def RequireEventTypes(self, *event_types):
    """Raise an exception if any of the event types is not present.

    :param List[type] event_types
    """
    for event_type in event_types:
      if len(self.FilteredByType(event_type)) == 0:
        raise Exception("Trace does not show the events of type " +
                        str(event_type))

  def HasEventTypes(self, *event_types):
    """Return True only if all event types are present.

    :param List[type] event_types
    :returns bool
    """
    for event_type in event_types:
      if len(self.FilteredByType(event_type)) == 0:
        return False
    return True

  def Trimmed(self, start, end):
    """Return a copy of the trace trimmed by start and end time.

    :param int start: start time of trim
    :param int end: end time of trim.
    :returns Trace
    """
    return Trace([e for e in self if start <= e.time < end], self.ms_per_frame)

  def Find(self, event_type, time, where="closest"):
    """Find first event of event_type around time.

    :param type event_type: event type to find.
    :param int time: target time around which to search.
    :param str where: either "closest", "before" or "after".
    """
    prev_event = None
    for event in self.FilteredByType(event_type):
      if event.time == time:
        return event
      elif event.time > time:
        if where == "closest":
          if prev_event is None:
            return event
          delta_this = event.time - time
          delta_prev = time - prev_event.time
          if delta_this > delta_prev:
            return prev_event
          else:
            return event
        elif where == "before":
          return prev_event
        elif where == "after":
          return event
      prev_event = event
    if where == "before":
      return prev_event
    return None

  def FindStateSwitch(self, event_type, target_state):
    """Return first event at which event_type switches to target_state.

    :param type event_type: Event type to find.
    :param bool target_state: Target state to find.
    """
    for event in self.FilteredByType(event_type):
      if event.state == target_state:
        return event
    return None

  def FindFingerCrossing(self, target_location, target_time):
    """Return time at which the finger crossed the location of target_event.

    :param float target_location: location which the finger should cross
    :param int target_time: time at which to start searching.
    """
    # Shift finger location to be zero at the target location and positive
    # at the target time.
    shifted = self.finger - target_location
    if shifted[target_time] < 0:
      shifted *= -1

    # Find zero crossing of shifted, which is when the finger is crossing.
    for i in range(max(len(shifted) - target_time, target_time)):
      left = target_time - i
      right = target_time + i
      if left > 0 and left < len(shifted) and shifted[left] <= 0:
        return FingerEvent(int(left), target_location)
      if right > 0 and right < len(shifted) and shifted[right] <= 0:
        return FingerEvent(int(right), target_location)

    return None

  def SegmentedByLineReset(self):
    """Segment into multiple traces separated by line reset events."""
    events = []
    for event in self:
      if isinstance(event, LineDrawEvent) and event.location is None:
        if events:
          yield Trace(events, self.ms_per_frame)
          events = []
      else:
        events.append(event)
    if events:
      yield Trace(events, self.ms_per_frame)

  def SegmentedByLED(self):
    """Segment into traces starting with each LED on event."""
    events = None
    for event in self:
      if isinstance(event, LEDEvent) and event.state == Event.STATE_ON:
        if events:
          yield Trace(events, self.ms_per_frame)
        events = []
      if events is not None:
        events.append(event)
    if events:
      yield Trace(events, self.ms_per_frame)

  def FilteredByType(self, event_type):
    """
    :param type event_type
    :returns Trace: filtered to only include events of event_type.
    """
    return Trace([e for e in self if isinstance(e, event_type)],
                 self.ms_per_frame)

  def FindLinearFingerMotion(self, debug=False):
    """Return (start, end) event of range in which event shows linear motion.

    Searches for the fastest motion of the finger and returns a (start, end)
    tuple of events indicating the range in which this motion is linear
    at this speed.
    :returns Tuple[Event, Event]
    """
    finger_location = self.uncalib_finger

    # Allow 20% variation of the maximum speed in linear range.
    speeds = self.uncalib_finger_speed
    speeds[np.isnan(speeds)] = 0

    max_speed_idx = np.argmax(speeds)
    min_speed_factor = (1 - self.LINEAR_MOTION_MAX_SPEED_DEVIATION)
    min_speed = speeds[max_speed_idx] * min_speed_factor

    # Find area in which the robot continuously has this speed.
    start = 0
    end = len(speeds) - 1
    for i, speed in enumerate(speeds):
      if speed > min_speed:
        if not start:
          start = i
      else:
        if start:
          end = i - 1
          break

    if debug:
      print "max_speed_idx=%d, range=(%d, %d)" % (max_speed_idx, start, end)
      pyplot.figure()
      pyplot.plot(finger_location)
      pyplot.figure()
      pyplot.plot(speeds)
      pyplot.show()

    return (FingerEvent(start, finger_location[start]),
            FingerEvent(end, finger_location[end]))

  def FindStationaryFinger(self, start_time, debug=False):
    """Return (start, end) event of range in which event shows no motion.

    The stationary range has to be enclosed between motion.
    :returns Tuple[Event, Event]
    """
    start = None
    end = None
    finger_speed = self.uncalib_finger_speed
    finger_location = self.uncalib_finger
    start = 0
    for i in range(start_time, 0, -1):
      speed = finger_speed[i]
      if np.isnan(speed):
        continue
      if speed < self.STATIONARY_MAX_SPEED:
        if not end:
          end = i
      else:
        if end:
          start = i + 1
          break
    if debug:
      print "start_time=%d, range=(%s, %s)" % (start_time, start, end)
      pyplot.figure()
      pyplot.plot(finger_location)
      pyplot.figure()
      pyplot.plot(finger_speed)
      pyplot.show()

    if start is None or end is None:
      raise Exception("Cannot find static motion range")
    return (FingerEvent(start, finger_location[start]),
            FingerEvent(end, finger_location[end]))

  def _TimeSeries(self, event_type, start_time=False, interpolate=False,
                  calib=False):
    """Turn list of events into a time series plot of this event.

    location based events will result in a location over time plot, whereas
    state-based events will be plotted as a step function, increasing by
    one for each positive state and decreasing for each negative state.

    :param type event_type: event type to turn into a time series
    :param bool start_time: Use events start time in plot instead of time.
    :param bool interpolate: Use linear interpolation between events instead of
           a step function.
    :param bool calib: Use calibrated values instead of raw values
    """
    if len(self) == 0:
      return np.empty((0,))

    filtered_events = self.FilteredByType(event_type)

    if event_type in (FingerEvent, LineDrawEvent, AnalogStateEvent):
      current_value = np.nan
    elif event_type in (LEDEvent, ScreenDrawEvent):
      current_value = 0
      if len(filtered_events):
        first_event = filtered_events[0]
        current_value = 0 if first_event.state else 1
    else:
      raise ValueError("Unknown event type: %s" % event_type)

    array = np.empty((self.end_time + 1,))
    array[:] = current_value
    last_time = 0

    for event in filtered_events:
      if start_time and event.start_time is not None:
        time = event.start_time if calib else event.uncalib_start_time
      else:
        time = event.time if calib else event.uncalib_time

      if event.state is not None:
        current_value += 1 if event.state else -1
      elif event.location is not None:
        if calib:
          current_value = event.location
        else:
          current_value = event.uncalib_location
      elif event.analog_state is not None:
        current_value = event.analog_state
      else:
        current_value = np.nan

      if time >= len(array):
        continue
      array[time] = current_value
      last_value = array[last_time]
      if not np.isnan(last_value):
        if interpolate:
          samples = time - last_time + 1
          array[last_time:(time + 1)] = np.linspace(last_value, current_value,
                                                    samples)
        else:
          array[(last_time + 1):time] = last_value
      last_time = time

    array[last_time:] = current_value
    return array

  def _CalculateSpeed(self, time_series):
    """Calculate a low pass filtered derivative of the time series.

    :param np.ndarray time_series
    :returns np.ndarray
    """
    # Calculated smoothed first derivative
    smoothing_kernel = self.SPEED_SMOOTHING_KERNEL
    def LowPass(f, N=smoothing_kernel):
      return signal.convolve(f, np.ones(N,) / N, "valid")

    if len(time_series) < smoothing_kernel * smoothing_kernel:
      # We can't smooth with too few samples
      return np.abs(np.diff(time_series))

    smoothed = LowPass(time_series)
    speed = np.abs(LowPass(np.diff(smoothed)))

    # Smoothing and deriving above reduces the array size, we have to account
    # for the shift in indices.
    reshaped = np.zeros((len(time_series),))
    reshaped[smoothing_kernel-1:-smoothing_kernel] = speed
    return reshaped
