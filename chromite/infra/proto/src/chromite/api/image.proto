syntax = "proto3";

package chromite.api;

option go_package = "go.chromium.org/chromiumos/infra/proto/go/chromite/api";

import "chromite/api/build_api.proto";
import "chromiumos/common.proto";

// Image argument - encapsulate data about an image.
message Image {
  // Path to the image file.
  string path = 1;
  // The image type.
  chromiumos.ImageType type = 2;
  // The build target used to create the image.
  chromiumos.BuildTarget build_target = 3;
}

// The image test arguments.
message CreateImageRequest {
  // Required.
  // The build target whose image is being built.
  chromiumos.BuildTarget build_target = 1;
  // The types of images to build, defaults to building base image.
  // Note: Building either of the VM image types will also force the
  // corresponding regular image to be built. Only one VM image type may be
  // built at a time, and will overwrite any previously built VM image if the
  // same output directory (version) is used.
  repeated chromiumos.ImageType image_types = 2;
  // Whether rootfs verification should be disabled (enabled by default).
  bool disable_rootfs_verification = 3;
  // The image version.
  string version = 4;
  // Disk layout option. See README.disk_layout and legacy_disk_layout.json in
  // src/scripts/build_library.
  string disk_layout = 5;
  // Used to set the LSB builder path key in /etc/lsb-release. See
  // chromite/scripts/cros_set_lsb_release.py.
  string builder_path = 6;
  // The chroot to use to execute the endpoint.
  chromiumos.Chroot chroot = 7;
}

message CreateImageResult {
  // Whether it completed successfully.
  bool success = 1;
  // Images that were built. Will contain no more than one per image type.
  repeated Image images = 2;
  // Any packages that failed to build. Empty on success.
  repeated chromiumos.PackageInfo failed_packages = 3;
}

// The image test arguments.
message TestImageRequest {
  // The image to be tested.
  // Image.path is required.
  Image image = 1;
  // The build target whose image is being tested.
  // BuildTarget.name is required.
  chromiumos.BuildTarget build_target = 2;

  // Test results options specifications.
  message Result {
    // Location where the test results should be written.
    string directory = 1;
  }
  // Required.
  Result result = 3;

  // The chroot to use to execute the endpoint.
  chromiumos.Chroot chroot = 4;
}

message TestImageResult {
  // Whether all tests passed.
  bool success = 1;
}

// The image service.
service ImageService {
  option (service_options) = {
    module: "image",
    service_chroot_assert: INSIDE,
  };

  // Build an image.
  // Example json:
  // {
  //  "build_target": {"name": "reef"},
  // }
  rpc Create(CreateImageRequest) returns (CreateImageResult);

  // Test an image.
  // Example json:
  // {
  //  "build_target": {"name": "reef"},
  //  "image": {"path": "/mnt/host/source/src/build/images/reef/latest"},
  //  "result": {"directory": "/tmp/image_test_results"}
  // }
  rpc Test(TestImageRequest) returns (TestImageResult);
}
