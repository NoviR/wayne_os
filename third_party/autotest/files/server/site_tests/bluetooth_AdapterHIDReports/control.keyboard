# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

from autotest_lib.server import utils

AUTHOR = 'chromeos-chameleon'
NAME = 'bluetooth_AdapterHIDReports.keyboard'
PURPOSE = ('Test bluetooth adapter receiving reports from '
           'bluetooth HID devices.')
CRITERIA = 'Adapter should receive HID events correctly.'
ATTRIBUTES = 'suite:bluetooth_flaky'
TIME = 'SHORT' # This test takes about 3 minutes
TEST_CATEGORY = 'Functional'
TEST_CLASS = 'bluetooth'
TEST_TYPE = 'server'
DEPENDENCIES = 'bluetooth, chameleon:bt_hid'

DOC = """
Verify that the bluetooth adapter of the DUT could receive HID reports
sent from a connected bluetooth device correctly.

Specifically, the following subtests are executed in this autotest.
    - test_keyboard_input
"""

args_dict = utils.args_to_dict(args)
chameleon_args = hosts.CrosHost.get_chameleon_arguments(args_dict)

def run(machine):
    host = hosts.create_host(machine, chameleon_args=chameleon_args)
    job.run_test('bluetooth_AdapterHIDReports', host=host, device_type='KEYBOARD',
                 num_iterations=1, min_pass_count=1)

parallel_simple(run, machines)
