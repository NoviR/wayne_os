# Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""
Yet another mini-library for creating tables from relational style datasets.

A PivotTable object keeps one or more multidimensional tables represented as
dicts. The keys are tuples like (dim1, dim2) and values are Cell objects of
different flavors used to do different kinds of aggregations.

"""

import collections
import itertools


class BaseCell(object):
    """
    Abstract base class for all sub-types of Cell objects.

    Deriving classes must override the add() method.

    """
    def __init__(self):
        # _add_count - count of how many times the add func was called.
        self._add_count = 0
        # self.value is used by the default template to format the Cell as <td>.
        self.value = None
        self.template = '<td>{value}</td>'

    def add(self, d):
        """
        Adds an element to this cell. Must be overridden in derived classes.

        @param d: can be absolutely anything and the body of the add() function
                is responsible for defining what "adding" means for a particular
                type of cell. In most cases d will be a dict representing a
                single row of data returned from an SQL query using DictCursor.

        """
        msg = 'Classes derived from BaseCell must override the add() method.'
        raise NotImplementedError(msg)

    def get_html(self, context={}):
        """Format the cell as HTML."""
        if hasattr(self, 'html'):
            return self.html

        d = self.__dict__.copy()
        d.update(context)
        if hasattr(self, 'format') and callable(self.format):
            return self.format(**d)
        elif hasattr(self, 'template'):
            return self.template.format(**d)
        raise Exception('No formatting methods specified for PivotTable cell.')


class TakeLastCell(BaseCell):
    """A Cell object that uses the last values added."""
    def __init__(self):
        super(TakeLastCell, self).__init__()

    def add(self, d):
        self._add_count += 1
        self.__dict__.update(d)


class ListCell(BaseCell):
    """Cell objects that keeps a list of everything added to it."""
    def __init__(self):
        super(ListCell, self).__init__()
        self.lst = []

    def add(self, d):
        self._add_count += 1
        self.lst.append(d)


class MultiListCell(TakeLastCell):
    """
    Keep data in separate lists, one list per field name ever added.

    It also updates the cell's __dict__ with the last dict added like the
    TakeLastCell. This type of cell is useful for tables populated from several
    different queries.

    """
    def __init__(self):
        super(MultiListCell, self).__init__()
        self.lists = {}

    def add(self, d):
        super(MultiListCell, self).add(d)
        for k in d:
            lst = self.lists.setdefault(k, [])
            lst.append(d[k])


class SumCell(BaseCell):
    """
    A Cell object that sums the added values.

    self.sum_fields is the list of fields that are summed, it must be set before
    the table is populated, usually set by the cell factory.

    """
    def __init__(self):
        super(SumCell, self).__init__()
        self.sum_fields = None

    def add(self, d):
        self._add_count += 1
        for k in d:
            if self.sum_fields and k not in self.sum_fields:
                continue
            if k in self.__dict__:
                self.__dict__[k] += d[k]
            else:
                self.__dict__[k] = d[k]


def make_cell_factory(cellclass, **params):
    """Create a function that produces Cell objects with pre-set properties."""
    def cell_factory():
        cell = cellclass()
        cell.__dict__.update(params)
        return cell
    return cell_factory


class PivotTable(object):
    """A pivot table for reshaping and aggregating data into a 2D table."""

    def __init__(self):
        self.groupings = []
        self.keylists = {}
        self.data = {}

    def add_grouping(self, key_fields, cell_factory, name=None):
        """
        Add a grouping.

        Normally called before adding any data. Each row added to the table,
        will be added to each grouping.

        @param key_fields: Either a single field name or a tuple of several
                field names. E.g. key_fields = ('platform', 'build'), in this
                case all added rows with the same platform and build will be
                added to the same cell in this grouping.
        @param cell_factory: A callable that produces new cell objects.
                E.g. cell_factory=ListCell.
        @param name: A name for this grouping, if omitted, key_fields joined
                with underscore will be used as the name.

        """
        # For 1D aggregations we accept field names not in tuple.
        if type(key_fields) in (str, int):
            key_fields = (key_fields,)
        if not name:
            name = '_'.join(str(f) for f in key_fields)
        self.groupings.append((key_fields, cell_factory, name))
        self.data[name] = collections.defaultdict(cell_factory)
        # All observed values of a key field are kept in keylists.
        for kf in key_fields:
            self.keylists.setdefault(kf, [])

    def _make_key(self, key_fields, d):
        if len(key_fields) == 1:
            key = d[key_fields[0]]
        else:
            key = tuple(d[f] for f in key_fields)
        return key

    def add_dict(self, d):
        """Add data row represented as a dict."""
        # Update keylists
        for kf in self.keylists:
            val = d[kf]
            keylist = self.keylists[kf]
            # keylist is not a set() because we care about order.
            if val not in keylist:
                keylist.append(val)
        # Add data row to each of the groupings.
        for key_fields, _, name in self.groupings:
            key = self._make_key(key_fields, d)
            self.data[name][key].add(d)

    def add(self, **d):
        """
        Add data to pivot table.

        Example:
            pt.add(release='R28', platform='lumpy', count=10)

        """
        self.add_dict(d)

    def add_dictlist(self, lst):
        """Add a bunch of data rows at once."""
        for d in lst:
            self.add_dict(d)

    def all_cells(self):
        """Get an iterator over ALL cells in all groupings."""
        it = itertools.chain(
                *(d.itervalues() for d in self.data.itervalues()))
        return it

    def cell_iterator(self, groupings):
        """Get an iterator over cells in given list of groupings."""
        it = itertools.chain(
                *(self.data[g].itervalues() for g in groupings))
        return it

# TODO (kamrik): consider merging this class into PivotTable.
class HtmlTable(object):
    """
    An object for rendering 2D data as HTML table.

    2D data should be represented as dict with (row, col) tuples as keys and
    some objects with get_html() method as values.

    """

    def __init__(self, rows, cols, data2d):
        self.rows = rows
        self.cols = cols
        self.data2d = data2d
        self.col_headers = []
        self.row_headers = []
        self.format_row_header = '<th> {} </th>'.format
        self.format_col_header = '<th> {} </th>'.format
        self.title = None
        self.title_class = ''
        self.table_id = None

    def html(self):
        """Generate the HTML code for this table, and return it as a string."""
        extra = ' id="%s"' % self.table_id if self.table_id else ''
        table = ['<table%s>' % extra]
        if self.title:
            colspan = len(self.cols) + len(self.row_headers) + 1
            table.append(
                '<tr><th colspan="%s" class="%s">%s</th></tr>'
                    % (colspan, self.title_class, self.title)
            )
        tr = ['<tr>']
        # Insert empty cells for each of the row headers.
        # TODO (kamrik): add some way to insert corner headers like "Total".
        for _ in xrange(len(self.row_headers) + 1):
            tr.append('<th></th>')
        # Main col header (col names).
        for col in self.cols:
            col_header = self.format_col_header(col)
            tr.append(col_header)
        tr.append('</tr>')
        table.extend(tr)

        # Secondary col headers.
        for header in self.col_headers:
            tr = ['<tr>']
            # Skip cells taken with row headers below.
            for _ in xrange(len(self.row_headers) + 1):
                tr.append('<td></td>')
            for col in self.cols:
                cell = header.get(col)
                tr.append(cell.get_html() if cell else '<td></td>')
            tr.append('</tr>')
            table.extend(tr)

        # rows with 2D data.
        for row in self.rows:
            tr = ['<tr>']
            # Main row header
            row_header = self.format_row_header(row)
            tr.append(row_header)
            # Additional headers.
            for header in self.row_headers:
                cell = header.get(row)
                tr.append(cell.get_html() if cell else '<td></td>')
            # 2D data.
            for col in self.cols:
                cell = self.data2d.get((row, col))
                tr.append(cell.get_html() if cell else '<td></td>')
            tr.append('</tr>')
            table.extend(tr)

        table.append('</table>')
        return '\n'.join(table)
