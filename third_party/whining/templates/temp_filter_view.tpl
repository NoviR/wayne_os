%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%from src import settings
%_root = settings.settings.relative_root

%def body_block():
  %# --------------------------------------------------------------------------
  %# Temporary (Custom) Filter View
  <script>
    app.controller('MyController', function($scope) {
      %# """ Defines the data model shared between functions and ui.
      %#
      %# This cannot be moved to a .js file because it uses
      %# server-side template directives to statically populate the
      %# data fields until we get client-side data retrieval working.
      %#
      %# This file uses both server-side templates and client-side
      %# (angular) templates. The server-side replacement uses {{}}
      %# while the client-side uses {[{}]}.
      %#
      %# Server-side comments use %# while client-side comments use //.
      %#
      %# Data:
      %#   data: holds all candidate data to match against.
      %#   query: the input string to match candidates against.
      %#   selected: holds clauses to be included in the temp filter.
      %#   daysBackOptions: options, including None, for setting days_back.
      %#   daysBackSelected: option for days_back if chosen.
      %#
      %# Functions:
      %#   filterQPs(): returns a list of matching candidates.
      %#
      %#   addSelected(): inserts selected clauses into selected.
      %#
      %# """
      %_fields = ['builds', 'platforms', 'releases', 'suites', 'tests']

      $scope.data = {
      %for _field in _fields:
          '{{ _field }}': {{! tpl_vars[_field] }},
      %end
      };

      $scope.query = null;

      $scope.selected = {
      %for _field in _fields:
          '{{ _field }}': [],
      %end
      };

      $scope.daysBackOptions = [{k:'', v:null}, {k:1, v:1}, {k:3, v:3},
                                {k:7, v:7}, {k:14, v:14}, {k:21, v:21},
                                {k:30, v:30}, {k:60, v:60}, {k:90, v:90},
                                {k:120, v:120}, {k:150, v:150}, {k:180, v:180}];
      $scope.daysBackSelected = $scope.daysBackOptions[0].v;

      $scope.filterQPs = function() {
        %# returns a list of matching candidates.
        var data = $scope.data;
        var filtered = {};
        var query = $scope.query;

        for (var k in data) {
          filtered[k] = [];
        }
        if (!query || String(query).trim().length == 0) {
          return;
        }
        var queryRegExp = RegExp(query, 'i'); //'i' -> case insensitive
        for (var k in data) {
          angular.forEach(data[k], function(v) {
            if (v.match(queryRegExp)) {
              filtered[k].push(v);
            }
          });
        }
        var filtered_list = [];
        for (var k in filtered) {
          for (var i=0; i<filtered[k].length; i++) {
            filtered_list.push(k + '=' + filtered[k][i]);
          }
        }
        return filtered_list;
      }

      $scope.addSelected = function(item) {
        %# inserts selected clauses into selected.
        var selected = $scope.selected;
        var keyval = item.split('=');
        if (selected[keyval[0]].indexOf(keyval[1]) == -1) {
          selected[keyval[0]].push(keyval[1]);
        }
      }

      $scope.removeSelected = function(selected_list, index) {
        selected_list.splice(index, 1);
        }

      $scope.openSelected = function() {
        var selected_qp_string = '';
        for (var k in $scope.selected) {
          if ($scope.selected[k].length > 0) {
            if (selected_qp_string.length > 0) {
              selected_qp_string += '&';
            }
            selected_qp_string += (k + '=');
          }
          angular.forEach($scope.selected[k], function(v) {
            if (selected_qp_string[selected_qp_string.length-1] != ['=']) {
              selected_qp_string += ',';
            }
            selected_qp_string += v
          });
        }
        if ($scope.daysBackSelected) {
          if (selected_qp_string.length > 0) {
            selected_qp_string += '&';
          }
          selected_qp_string += 'days_back=' + $scope.daysBackSelected;
        }
        if (selected_qp_string.length > 0) {
          open('{{ _root }}/unfiltered?' + selected_qp_string);
        }
      }
    });
  </script>

  <h3>Define a Temporary Filter</h3>
  <p>
    You can see test result views based on <b>your own criteria</b>
    by defining <a href="{{ _root }}/filters">filters</a>.<br>
    Use this page to supply the build numbers, release numbers,
    suite names and/or test names that need to be included.<br>
    Full lookup lists are available as suggestions:
    %for _field in _fields:
        <a href="{{ _root }}/{{ _field }}" target="_blank">{{ _field }}</a>&nbsp;
    %end
    <br>
    Try typing an example (
    %for _field in _fields:
      {{ _field }}&nbsp;
    %end
    ) here:<br>
  </p>

  <div ng-controller="MyController">
    <input ng-model="query" size=80>
    <a class="maia-button" ng-click="openSelected()">
      Jump to my custom view
    </a>
    <br>
    Days in the past to consider:
    <select ng-model='daysBackSelected' required
            ng-options='option.v as option.k for option in daysBackOptions'>
    </select>
    <hr>
    %for _field in _fields:
        <div ng-repeat="field in selected.{{ _field }}">
          [ <a href="" ng-click="removeSelected(selected.{{ _field }}, $index)">X</a> ]
          {{ _field }}={[{ field }]}
        </div>
    %end
    <hr>
    <ul>
      <li class="infotext" ng-repeat="item in filterQPs()">
        [ <a href="" ng-click="addSelected(item)">add</a> ]
        {[{ item }]}
      </li>
    </ul>
  </div>
%end

%rebase('master.tpl', title='temp filters', query_string=query_string, body_block=body_block, angular=True)
