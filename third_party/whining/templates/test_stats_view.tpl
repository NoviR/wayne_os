%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%from src import settings
%_root = settings.settings.relative_root


%def body_block():
  <h2 style="text-align:center">
    Chromium OS Test Results (Whining) - 2 week stats for {{ tpl_vars['data']['test_name'] }} on R{{ tpl_vars['data']['release'] }}
  </h2>
  %if not tpl_vars['data'].get('platforms'):
    <h3>
      <u>
        No runs found on on R{{ tpl_vars['data']['release'] }}
        for {{ tpl_vars['data']['test_name'] }}.
      </u>
    </h3>
  %end

  %# --------------------------------------------------------------------------
  %# Stats
  %grand_total = tpl_vars['data']['grand_total']
  <div id="divStats" align="center">
    <table class="alternate_background bordereditem">
      <tbody>
        <tr>
          <th class="bordereditem headeritem">Reason</th>
          %for platform in tpl_vars['data']['platforms']:
            <th class="tableheader bordereditem" valign="bottom" style="height: 5em;">
              <div class="div_testname">
                &nbsp;
                <a class="tooltip"
                onmouseover="fixTooltip(event, this)"
                onmouseout="clearTooltip(this)"
                href="{{ _root }}/failures/{{tpl_vars['filter_tag']}}{{! query_string(add={'platforms':platform}) }}">
                  {{platform}}
                  <span>
                    Show failures for platform={{platform}} only.
                  </span>
                </a>
              </div>
            </th>
          %end
          <th> Total </th>
        </tr>
        %for reason in tpl_vars['data']['reasons']:
          <tr>
            <td class="bordereditem">{{reason}}</td>
            %for platform in tpl_vars['data']['platforms']:
               %cnt = tpl_vars['data']['counts'].get((platform, reason))
              <td class="bordereditem">{{cnt if cnt is not None  else ''}}</td>
            %end
            %cnt = tpl_vars['data']['reason_counts'][reason]
            <td class="bordereditem"> {{tpl_vars['data']['reason_counts'][reason]}}
              ({{int(cnt *100. / tpl_vars['data']['grand_total']) }}%)
            </td>
          </tr>
        %end
        <tr>
          <td class="bordereditem">Total runs</td>
          %for platform in tpl_vars['data']['platforms']:
            %cnt = tpl_vars['data']['platform_counts'][platform]
            <td class="bordereditem">{{ cnt if cnt is not None else '' }}</td>
          %end
          <td class="bordereditem">{{ tpl_vars['data']['grand_total'] }}</td>
        </tr>
      </tbody>
    </table>
  </div>
  <hr>
%end

%rebase('master.tpl', title='builds', query_string=query_string, body_block=body_block)
